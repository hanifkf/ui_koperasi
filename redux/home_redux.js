import {guardInstance} from "../utils/axiosConfig";
import {message} from "antd";
import {takeLatest, put} from "redux-saga/effects";
import {act} from "react-dom/test-utils";


const actionTypes = {
    LOAD_REKAP_SIMPANAN_WAJIB: 'LOAD_REKAP_SIMPANAN_WAJIB',
    REKAP_SIMPANAN_WAJIB_LOADED: 'REKAP_SIMPANAN_WAJIB_LOADED',
    LOAD_REKAP_SIMPANAN_POKOK: 'LOAD_REKAP_SIMPANAN_POKOK',
    REKAP_SIMPANAN_POKOK_LOADED: 'REKAP_SIMPANAN_POKOK_LOADED',
    LOAD_REKAP_PENGELUARAN: 'LOAD_REKAP_PENGELUARAN',
    REKAP_PENGELUARAN_LOADED: 'REKAP_PENGELUARAN_LOADED',
    LOAD_REKAP_TOTAL_PINJAMAN: 'LOAD_REKAP_TOTAL_PINJAMAN',
    REKAP_TOTAL_PINJAMAN_LOADED: 'REKAP_TOTAL_PINJAMAN_LOADED',
    LOAD_REKAP_TOTAL_ADMINISTRASI: 'LOAD_REKAP_TOTAL_ADMINISTRASI',
    REKAP_TOTAL_ADMINISTRASI_LOADED: 'REKAP_TOTAL_ADMINISTRASI_LOADED',
    LOAD_REKAP_ANGSURAN_BERJALAN: 'LOAD_REKAP_ANGSURAN_BERJALAN',
    REKAP_ANGSURAN_BERJALAN_LOADED: 'REKAP_ANGSURAN_BERJALAN_LOADED',
    LOAD_REKAP_ANGSURAN_LUNAS: 'LOAD_REKAP_ANGSURAN_LUNAS',
    REKAP_ANGSURAN_LUNAS_LOADED: 'REKAP_ANGSURAN_LUNAS_LOADED',
    LOAD_REKAP_ADMIN_BUNGA_LUNAS: 'LOAD_REKAP_ADMIN_BUNGA_LUNAS',
    REKAP_ADMIN_BUNGA_LUNAS_LOADED: 'REKAP_ADMIN_BUNGA_LUNAS_LOADED',
}

const initialState = {
    status: false,
    loading: false,
    rekapSimpananWajib: [],
    rekapSimpananPokok: 0,
    rekapPengeluaran: 0,
    rekapTotalPinjaman: 0,
    rekapTotalAdministrasi: 0,
    rekapAngsuranBerjalan: 0,
    rekapAngsuranLunas: 0,
    rekapAdminBungaLunas: 0,
}

export const homeReducer = function (state = initialState, {type, payload}) {
    switch (type) {
        case actionTypes.LOAD_REKAP_SIMPANAN_WAJIB: {
            return {
                ...state
            }
        }
        case actionTypes.REKAP_SIMPANAN_WAJIB_LOADED: {
            return {
                ...state,
                rekapSimpananWajib: payload
            }
        }
        case actionTypes.LOAD_REKAP_PENGELUARAN: {
            return {
                ...state,
            }
        }
        case actionTypes.REKAP_PENGELUARAN_LOADED: {
            return {
                ...state,
                rekapPengeluaran: payload
            }
        }
        case actionTypes.LOAD_REKAP_TOTAL_PINJAMAN: {
            return {
                ...state,
            }
        }
        case actionTypes.REKAP_TOTAL_PINJAMAN_LOADED: {
            return {
                ...state,
                rekapTotalPinjaman: payload
            }
        }
        case actionTypes.LOAD_REKAP_TOTAL_ADMINISTRASI: {
            return {
                ...state
            }
        }
        case actionTypes.REKAP_TOTAL_ADMINISTRASI_LOADED: {
            return {
                ...state,
                rekapTotalAdministrasi: payload
            }
        }
        case actionTypes.LOAD_REKAP_ANGSURAN_BERJALAN: {
            return {
                ...state
            }
        }
        case actionTypes.REKAP_ANGSURAN_BERJALAN_LOADED: {
            return {
                ...state,
                rekapAngsuranBerjalan: payload
            }
        }
        case actionTypes.LOAD_REKAP_ANGSURAN_LUNAS: {
            return {
                ...state,
                rekapAngsuranLunas: payload
            }
        }
        case actionTypes.LOAD_REKAP_ADMIN_BUNGA_LUNAS: {
            return {
                ...state
            }
        }
        case actionTypes.REKAP_ADMIN_BUNGA_LUNAS_LOADED: {
            return {
                ...state,
                rekapAdminBungaLunas: payload
            }
        }
        default: {
            return state
        }
    }
}

export const homeDispatch = {
    loadRekapSimpananWajib: (data) => ({
        type: actionTypes.LOAD_REKAP_SIMPANAN_WAJIB,
        payload: data
    }),
    rekapSimpananWajibLoaded: (data) => ({
        type: actionTypes.REKAP_SIMPANAN_WAJIB_LOADED,
        payload: data
    }),
    loadRekapSimpananPokok: (data) => ({
        type: actionTypes.LOAD_REKAP_SIMPANAN_POKOK,
        payload: data
    }),
    rekapSimpananPokokLoaded: (data) => ({
        type: actionTypes.REKAP_SIMPANAN_POKOK_LOADED,
        payload: data
    }),
    loadRekapPengeluaran: (data) => ({
        type: actionTypes.LOAD_REKAP_PENGELUARAN,
        payload: data
    }),
    rekapPengeluaranLoaded: (data)=>({
        type: actionTypes.REKAP_PENGELUARAN_LOADED,
        payload: data
    }),
    loadRekapTotalPinjaman: (data) => ({
        type: actionTypes.LOAD_REKAP_TOTAL_PINJAMAN,
        payload: data
    }),
    rekapTotalPinjamanLoaded: (data) => ({
        type: actionTypes.REKAP_TOTAL_PINJAMAN_LOADED,
        payload: data
    }),
    loadRekapTotalAdminsitrasi: (data) => ({
        type: actionTypes.LOAD_REKAP_TOTAL_ADMINISTRASI,
        payload: data
    }),
    rekapTotalAdministrasiLoaded: (data) => ({
        type: actionTypes.REKAP_TOTAL_ADMINISTRASI_LOADED,
        payload: data
    }),
    laodRekapAngsuranBerjalan: (data) => ({
        type: actionTypes.LOAD_REKAP_ANGSURAN_BERJALAN,
        payload: data
    }),
    rekapAngusranBerjalan: (data) => ({
        type: actionTypes.REKAP_ANGSURAN_BERJALAN_LOADED,
        payload: data
    }),
    loadRekapAngsuranLunas: (data) => ({
        type: actionTypes.LOAD_REKAP_ANGSURAN_LUNAS,
        payload: data
    }),
    rekapAngsuranLunasLoaded: (data) => ({
        type: actionTypes.REKAP_ANGSURAN_LUNAS_LOADED,
        payload: data
    }),
    loadRekapAdminBungaLunas: (data) => ({
        type: actionTypes.LOAD_REKAP_ADMIN_BUNGA_LUNAS,
        payload: data
    }),
    rekapAdminBungaLunasLoaded: (data) => ({
        type: actionTypes.REKAP_ADMIN_BUNGA_LUNAS_LOADED,
        payload: data
    })
}

export function* saga() {
    const baseUrl = process.env.baseUrl

    yield takeLatest(actionTypes.LOAD_REKAP_SIMPANAN_WAJIB, function* ({payload}) {
        try {
            const {data: response} = yield guardInstance(payload.token).get(`${baseUrl}/api/v1/simpanan/simpanan-wajib/rekap/${payload.tahun}`)
            console.log(response)
            yield put(homeDispatch.rekapSimpananWajibLoaded(response.rekap1))
        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_SIMPANAN_POKOK, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_PENGELUARAN, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_TOTAL_PINJAMAN, function* ({payload}){
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_TOTAL_ADMINISTRASI, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_ANGSURAN_BERJALAN, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_ANGSURAN_LUNAS, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })

    yield takeLatest(actionTypes.LOAD_REKAP_ADMIN_BUNGA_LUNAS, function* ({payload}) {
        try {

        }catch (e) {
            message.error(e.message);
            console.log(e.response);
        }
    })
}

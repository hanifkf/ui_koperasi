import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useSession, signOut , getSession } from "next-auth/react"
import {useRouter} from "next/router";
import axios from "axios";
import Layout from "../component/layout";
import LayoutKu from "../component/layout";
import {Card, Col, Row, Table} from "antd";
import {useEffect, useState} from "react";
import {homeDispatch} from "../redux/home_redux";
import {connect} from "react-redux";
import {formatRupiah} from "../utils/rupiahFormat";

const Home = (props) => {
  const router = useRouter();
  const { data: session, status } = useSession();
  const [tahun, setTahun] = useState('');
  console.log('props, ',props)
  console.log(session)
  const simpananColumns = [
    {
      title: 'No',
      dataIndex: 'no',
      key: 'no',
      render: (text, record, index) => (
          <>{index+1}</>
      )
    },
    {
      title: 'Bulan',
      dataIndex: 'bulan',
      key: 'bulan',
      render: (text, record) => (
          <>{record.bulan}-{record.tahun}</>
      )
    },
    {
      title: 'Jumlah',
      dataIndex: 'total_amount',
      key: 'total_amount',
      render: (text, record) => (
          <>{formatRupiah(record.total_amount)}</>
      )
    }
  ]
  useEffect(()=>{
    props.loadRekapSimpananWajib({
      token: session.token,
      tahun: '2022',
    })
  },[])
  return (
    <Row gutter={6}>
      <Col span={12} >
        <Row>
          <Col span={24}>
            <Card title={'Simpanan Wajib'}>
              <Row>
                <Col span={24}>
                  <Table
                      columns={simpananColumns}
                      dataSource={props.rekapSimpananWajib}
                      pagination={false}
                  />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        <Row style={{marginTop: '6px'}}>
          <Col span={24}>
            <Card title={'Simpanan Pokok'}>

            </Card>
          </Col>
        </Row>
      </Col>
      <Col span={12}>
        <Row>
          <Col span={24}>
            <Card title={'Pemasukan Pinjaman'} >

            </Card>
          </Col>
        </Row>
        <Row style={{marginTop: '6px'}}>
          <Col span={24}>
            <Card title={'Pemasukan Pinjaman'} >

            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

Home.getLayout = function getLayout(page) {
  return(
      <LayoutKu>
        {page}
      </LayoutKu>
  )
}

// export async function getServerSideProps (context) {
//   const {data: data} = await axios.get('https://jsonplaceholder.typicode.com/todos/1')
//   const session = await getSession(context)
//   console.log('my=session, ',session)
//   console.log(data)
//   return {
//     props: {
//       name: "hanif",
//       data: data
//     }, // will be passed to the page component as props
//   }
// }

Home.auth = true

const mapStateToProps = (state) => {
  return{
    status: state.home.status,
    rekapSimpananWajib: state.home.rekapSimpananWajib
  }
}

export default connect(mapStateToProps, homeDispatch)(Home)
